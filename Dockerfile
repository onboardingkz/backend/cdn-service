FROM node:16

WORKDIR /srv/cdn

COPY ./package.json ./
COPY ./yarn.lock ./ 

RUN npm install

COPY . .

RUN npm run build

EXPOSE 3002

CMD ["npm", "run", "start"]
