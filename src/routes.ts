import {Express} from "express";
import {remove, upload} from "./handlers";

export const routes = (app: Express) => {
    app.post('/upload', upload);
    app.delete('/remove', remove);
};