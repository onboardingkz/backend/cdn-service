import {Request} from "express";

export const authenticate = (req: Request) => {
    const authHeader = req.headers.authorization?.split(" ");
    if (authHeader?.length === 2) {
        const [, token] = authHeader;
        if (token === process.env.AUTH_TOKEN) {
            return true;
        }
        return null;
    } else {
        return null;
    }
}