import {Request, Response} from "express";
import * as fs from "fs";
import {authenticate} from "../utils";

export const remove = async (req: Request, res: Response) => {
    if (authenticate(req)) {
        const file = req.query.file;

        if (file) {
            try {
                fs.unlinkSync(`uploads/${file}`);
                console.log(`DELETED | uploads/${file}`);
                return res.json({
                    message: "ok",
                });
            } catch (err) {
                return res.status(500).send(err.code === "ENOENT" ? { message: "already_deleted" } : err);
            }
        } else {
            return res.status(422).json({
                message: "no_file",
            });
        }
    } else {
        return res.status(401).json({
            message: "unauthorized",
        });
    }
}