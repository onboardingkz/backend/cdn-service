import {Request, Response} from "express";
import {authenticate, generateToken, getCategory} from "../utils";

export const upload = async (req: Request, res: Response) => {
    try {
        if (authenticate(req)) {
            const category = getCategory(req);
            if(!req.files) {
                res.status(422).json({
                    message: 'no_file'
                });
            } else {
                const file = req.files?.file;

                if (file) {
                    if (!Array.isArray(file)) {
                        const newFileName = `${generateToken(15)}${file.name}`;
                        console.log(`UPLOAD | ./uploads/${category}` + newFileName)
                        file.mv(`./uploads/${category}` + newFileName)
                            .then(() => {
                                return res.send(`${category}${newFileName}`);
                            })
                            .catch((err) => {
                                return res.status(500).send(err);
                            });
                    } else {
                        return res.status(422).json({
                            message: "single_file",
                        });
                    }
                } else {
                    return res.status(422).json({
                        message: 'no_file'
                    });
                }
            }
        } else {
            return res.status(401).json({
                message: "unauthorized",
            });
        }
    } catch (err) {
        res.status(500).send(err);
    }
}